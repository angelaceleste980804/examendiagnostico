﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ExamenDiagnostico
{
    public class proveedor
    {
        private int idprov;
        private string nombreprov;
        private string direccion;
        private string telefono;

        public int Idprov { get => idprov; set => idprov = value; }
        public string Nombreprov { get => nombreprov; set => nombreprov = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Telefono { get => telefono; set => telefono = value; }
    }
}
