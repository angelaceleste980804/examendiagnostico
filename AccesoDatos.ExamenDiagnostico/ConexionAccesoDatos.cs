﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.ExamenDiagnostico
{
    public class ConexionAccesoDatos
    {
        private MySqlConnection conn;
        public ConexionAccesoDatos(string servidor, string usuario, string password, string database, uint puerto)
        {
            MySqlBaseConnectionStringBuilder cadenaConexion = new MySqlConnectionStringBuilder();
            cadenaConexion.Server = servidor;
            cadenaConexion.UserID = usuario;
            cadenaConexion.Password = password;
            cadenaConexion.Database = database;
            cadenaConexion.Port = puerto;
            conn = new MySqlConnection(cadenaConexion.ToString());
        }

        /*public void EjecutarConsulta(string consulta)
        {
            try
            {
                conn.Open();
                var command = new MySqlCommand(consulta, conn);
                command.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }*/
        public void EjecutarConsulta(string consulta)
        {
            try
            {
                conn.Open();
                var command = new MySqlCommand(consulta, conn);
                command.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


        public DataSet obtenerDatos(string consulta, string tabla)
        {
            var ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter(consulta, conn);
            da.Fill(ds, tabla);
            return ds;

        }
        public DataSet obtenerDatos1(string consulta)
        {
            var ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter(consulta, conn);
            da.Fill(ds);
            return ds;

        }
    }
}
