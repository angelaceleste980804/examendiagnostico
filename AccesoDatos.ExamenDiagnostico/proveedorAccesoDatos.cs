﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ExamenDiagnostico;

namespace AccesoDatos.ExamenDiagnostico
{
    public class proveedorAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public proveedorAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "abarrotespueblito", 3306);
        }

        public List<proveedor> GetProveedors()
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listprov = new List<proveedor>();
            var ds = new DataSet();
            string consulta = "select * from proveedor;";

            //ds = conexion.obtenerDatos(consulta, "alumno");
            ds = conexion.obtenerDatos(consulta, "proveedor");
            var DataTable = new DataTable();
            DataTable = ds.Tables[0];

            foreach (DataRow row in DataTable.Rows)
            {
                var proveedor = new proveedor
                {
                    Idprov = Convert.ToInt32(row["idprov"]),
                    Nombreprov = row["nombreprov"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString()

                };

                listprov.Add(proveedor);

            }
            return listprov;
        }

        public void Guardar(proveedor proveedor)
        {
            if (proveedor.Idprov == 0)
            {
                string consulta = string.Format("insert into proveedor values(null,'{0}','{1}',{2})", proveedor.Nombreprov, proveedor.Direccion, proveedor.Telefono);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("update proveedor set nombreprov='{0}',direccion='{1}',telefono='{2}'", proveedor.Nombreprov, proveedor.Direccion, proveedor.Telefono);
                conexion.EjecutarConsulta(consulta);
            }
        }

        public void Eliminar(int control)
        {
            string consulta = string.Format("delete from proveedor where idprov={0}", control);
            conexion.EjecutarConsulta(consulta);
        }
    }
}
