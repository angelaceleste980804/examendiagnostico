create database abarrotespueblito;
create table proveedor(
idprov int auto_increment primary key,
nombreprov varchar(100),
direccion varchar(100),
telefono varchar(20));

create table categoria(
idcat int auto_increment primary key,
nombrecat varchar(50));

create table producto(
idprod int auto_increment primary key,
nombre varchar(50),
fkidcat int,
foreign key (fkidcat) references categoria(idcat));

create table relacion(
idrelacion int auto_increment primary key,
cantidad int,
precio double,
fecha varchar(50),
fkproducto int,
fkproveedor int,
foreign key (fkproducto) references producto(idprod),
foreign key (fkproveedor) references proveedor (idprov));

insert into proveedor values(null,'Sabritas','palma #12','788-725-52-58');
insert into categoria values(null,'Papas');
insert into producto values(null,'Doritos',2);
insert into relacion values(null,20,12,'20-02-20',2,2);

select * from relacion;

create view v_pedido as
select nombreprov as "Proveedor",nombrecat as "Categoria",nombre as 'Nombre',precio as '$',cantidad as 'Cant',fecha as 'Fecha' from relacion, proveedor,
categoria,producto where fkproducto=idprod and fkproveedor= idprov and fkidcat=idcat;

select * from v_pedido;

drop procedure validar_Cant;
create procedure validar_Cant(in _cantidad int,in _precio double,in _fecha varchar(20),in _fkprov int, in _fkprod int)
begin

 if _cantidad != null or _cantidad !=0 then
 insert into relacion values(null,_cantidad,_precio,_fecha,_fkprov,_fkprod);
 end if;
end ;

delete from relacion where idrelacion=3;

call validar_Cant(8,30,'25-02-20',1,1);
