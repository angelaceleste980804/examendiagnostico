﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ExamenDiagnostico;
using AccesoDatos.ExamenDiagnostico;

namespace LogicaNegocio.ExamenDiagnostico
{
    public class proveedorManejador
    {
        private proveedorAccesoDatos _proveedorAccesoDatos = new proveedorAccesoDatos();

        public List<proveedor> GetProveedors()
        {
            var listgrupo = _proveedorAccesoDatos.GetProveedors();
            return listgrupo;
        }
        public void Eliminar(int control)
        {
            _proveedorAccesoDatos.Eliminar(control);
        }
        public void Guardar(proveedor proveedor)
        {
            _proveedorAccesoDatos.Guardar(proveedor);
        }
    }
}
